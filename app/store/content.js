export const state = () => ({
  jumbotronVideoUrl: 'https://www.youtube-nocookie.com/embed/DLzxrzFCyOs?autoplay=1&mute=1&cc_load_policy=0&fs=0&loop=1&modestbranding=1&rel=0&showinfo=0&enablejsapi=1',
  isJumbotronVideoPlaying: false,
});

export const mutations = {
  playJumbotron(state) {
    state.isJumbotronVideoPlaying = true
  },
  stopJumbotron(state) {
    state.isJumbotronVideoPlaying = false
  }
};
